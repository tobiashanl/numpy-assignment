const Z_MIN = -1;
const Z_MAX = 4;
const BIN_WIDTH = 5;

const points = [[-2, 1, 1], [6, -1, 1], [1, 1, 2], [-4, 3, 1], [3, 0, 1], [0, 0, 0], [5, -4, 0], [1, 1, 5]];
// const points = [[-10, 1, 0], [2, -14, 0], [-1, -2, 3]];
// const points = [[15, -10, 0], [22, -12, 0], [12, -9, 0]];
// const points = [[1, 1, 0], [0.5, -1.5, 0], [-2, 0.5, 0], [0.3, 0.7, 1.4], [2.5, 1.3, -1.5]];

const histogram = getHistogram(points, BIN_WIDTH, Z_MIN, Z_MAX);

console.log('Input points (x,y,z):');
console.log(points);
console.log('Bin width:', BIN_WIDTH);
console.log('Z-Range:', Z_MIN, '< z <', Z_MAX);
console.log('Top left corner of histogram: x =', histogram.topLeftCorner.x, 'y =', histogram.topLeftCorner.y);
console.log('Histogram:');
console.log(histogram.histogram);

/*
All bin intervals except the last ones are half open, meaning a value on an edge
will get assigned to the bin on the top and right
*/
function getHistogram(points, binWidth, zMin, zMax) {
    points = points.filter(p => p[2] > zMin && p[2] < zMax);

    const edges = getOuterEdges(points, binWidth);
    const numberOfBins = getNumberOfBins(edges, binWidth);

    const histogram = Array(numberOfBins.y).fill(0).map(v => Array(numberOfBins.x).fill(0));
    points.forEach(p => {
        const yBin = Math.min(numberOfBins.y - 1, Math.floor((p[1] - edges.bottom) / binWidth));
        const xBin = Math.min(numberOfBins.x - 1, Math.floor((p[0] - edges.left) / binWidth));
        histogram[yBin][xBin] += 1;
    });

    return {histogram: histogram.reverse(), topLeftCorner: {x: edges.left, y: edges.top}};
}

function getOuterEdges(points, binWidth) {
    const xValues = points.map(p => p[0]);
    const yValues = points.map(p => p[1]);

    const left = Math.floor(Math.min(...xValues) / binWidth) * binWidth;
    const right = Math.ceil(Math.max(...xValues) / binWidth) * binWidth;
    const bottom = Math.floor(Math.min(...yValues) / binWidth) * binWidth;
    const top = Math.ceil(Math.max(...yValues) / binWidth) * binWidth;

    return {left, right, bottom, top};
}

function getNumberOfBins(edges, binWidth) {
    // Rounding is necessary here because of floating point precision issues with non-integer binWidths
    const x = Math.round((edges.right - edges.left) / binWidth);
    const y = Math.round((edges.top - edges.bottom) / binWidth);
    return {x, y}
}
