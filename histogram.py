import numpy as np
import math

Z_MIN = -1
Z_MAX = 4
BIN_WIDTH = 5


def get_number_of_bins(left_edge, right_edge, bottom_edge, top_edge, bin_width):
    # Rounding is necessary here because of floating point precision issues with non-integer binWidths
    number_of_x_bins = round((right_edge - left_edge) / bin_width)
    number_of_y_bins = round((top_edge - bottom_edge) / bin_width)
    return number_of_x_bins, number_of_y_bins


def get_outer_edges(x_values, y_values, bin_width):
    left_edge = math.floor(x_values.min() / bin_width) * bin_width
    right_edge = math.ceil(x_values.max() / bin_width) * bin_width
    bottom_edge = math.floor(y_values.min() / bin_width) * bin_width
    top_edge = math.ceil(y_values.max() / bin_width) * bin_width
    return left_edge, right_edge, bottom_edge, top_edge


def get_histogram(points, bin_width, z_min, z_max):
    """
    All bin intervals except the last ones are half open, meaning a value on an edge
    will get assigned to the bin on the top and right
    """
    points = points[((points[:, 2] > z_min) & (points[:, 2] < z_max))]
    x_values = points[:, 0]
    y_values = points[:, 1]

    left_edge, right_edge, bottom_edge, top_edge = get_outer_edges(x_values, y_values, bin_width)
    number_of_x_bins, number_of_y_bins = get_number_of_bins(left_edge, right_edge, bottom_edge, top_edge, bin_width)

    histogram, a, b = np.histogram2d(x_values, y_values, [number_of_x_bins, number_of_y_bins],
                                     [[left_edge, right_edge], [bottom_edge, top_edge]])
    return histogram, left_edge, top_edge


if __name__ == "__main__":
    points = np.array([[-2, 1, 1], [6, -1, 1], [1, 1, 2], [-4, 3, 1], [3, 0, 1], [0, 0, 0], [5, -4, 0], [1, 1, 5]])
    # points = np.array([[-10, 1, 0], [2, -14, 0], [-1, -2, 3]])
    # points = np.array([[15, -10, 0], [22, -12, 0], [12, -9, 0]])
    # points = np.array([[1, 1, 0], [0.5, -1.5, 0], [-2, 0.5, 0], [0.3, 0.7, 1.4], [2.5, 1.3, -1.5]])

    histogram, top_left_x_coordinate, top_left_y_coordinate = get_histogram(points, BIN_WIDTH, Z_MIN, Z_MAX)

    print('Input points (x,y,z):')
    print(points)
    print('Bin width:', BIN_WIDTH)
    print('Z-Range:', Z_MIN, '< z <', Z_MAX)
    print('Top left corner of histogram: x =', top_left_x_coordinate, 'y =', top_left_y_coordinate)
    print('Histogram:')
    print(np.flipud(histogram.transpose()))  # Transform numpy convention to cartesian convention
